public class App {
    public static void main(String[] args) throws Exception {
        CircleClass circle1 = new CircleClass();
        CircleClass circle2 = new CircleClass(3.0);

        System.out.println(circle1.toString());
        System.out.println(circle2.toString());
        //print area of circle1
        System.out.println(circle1.getArea());
        //print circumference of circle 1
        System.out.println(circle1.getCircumference());
        //print area of circle2
        System.out.println(circle2.getArea());
        //print circumference of circle 2
        System.out.println(circle2.getCircumference());
    }
}
