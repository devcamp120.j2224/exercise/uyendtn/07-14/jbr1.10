public class CircleClass {
    double radius = 1.0;

    public CircleClass(double radius) {
        this.radius = radius;
    }

    public CircleClass() {
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return Math.PI*radius*radius;
    }

    public double getCircumference() {
        return Math.PI*radius*2;
    }

    public String toString() {
        return "Circle[radius = " + this.radius + "]";
    }
}
